package com.mybatis.test.usermgmt;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybatis.test.usermgmt.model.UserDTO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUserMgmtApp extends UsermgmtApplicationTests {
	
	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void addUserAPI() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/users")
				.content(asJsonString(new UserDTO("23", "Admin", null, "Jiten", "Adhi", "C:\\Users\\JAI\\Desktop\\Aeris Folder\\Aeris Docs\\JSA\\Camera\\IMG_20170324_202033.jpg", "English", 1)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}
	
	@Test
	public void modifyUserAPI() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.put("/users")
				.content(asJsonString(new UserDTO("23", null, "Admin", "Jiten", "Adhikari", "C:\\Users\\JAI\\Desktop\\Aeris Folder\\Aeris Docs\\JSA\\Camera\\IMG_20170324_202033.jpg", "English", 1)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void getUserWithId() throws Exception {
		//Thread.sleep(10000);
		mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}","23"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.firstName").value("Jiten"))
				.andExpect(jsonPath("$.lastName").value("Adhi"))
				.andExpect(jsonPath("$.preferredLanguage").value("English"));

	}
	
	@Test
	public void removeUserAPI() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.delete("/users/{id}", "23") )
	        .andExpect(status().isAccepted());
	}
	
	private String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
