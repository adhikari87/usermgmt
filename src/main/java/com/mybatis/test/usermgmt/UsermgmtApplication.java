package com.mybatis.test.usermgmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mybatis.test.usermgmt.repository.UserRepository;

@SpringBootApplication
public class UsermgmtApplication {
	public static void main(String[] args) {
		SpringApplication.run(UsermgmtApplication.class, args);
	}

}
