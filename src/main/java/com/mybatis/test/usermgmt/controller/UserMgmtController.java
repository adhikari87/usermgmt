package com.mybatis.test.usermgmt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mybatis.test.usermgmt.entity.User;
import com.mybatis.test.usermgmt.model.UserDTO;
import com.mybatis.test.usermgmt.service.UserService;

/**
 * Rest controller for CRUD APIs
 * @author JAI
 *
 */
@RestController
public class UserMgmtController {

	@Autowired
	private UserService userService;
	
	@GetMapping(path="/users", produces = "application/json")
	public List<UserDTO> findAllUsers() {
		return userService.findAll();
	}
	
	@GetMapping(path="/users/{id}", produces = "application/json")
	public UserDTO findAllUsers(@PathVariable(value="id") String id) {
		return userService.findById(id);
	}
	
	@PostMapping(path= "/users", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addUser(@RequestBody UserDTO userDTO) {
        userService.addUser(userDTO);
        return new ResponseEntity<Object>("User created successfully",HttpStatus.CREATED);
    }
	
	@PutMapping(path= "/users", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateUser(@RequestBody UserDTO userDTO) {
        userService.updateUser(userDTO);
        return new ResponseEntity<Object>("User updated successfully",HttpStatus.OK);
    }
	
	@DeleteMapping(path= "/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable(value="id") String id) {
        userService.deleteById(id);
        return new ResponseEntity<Object>("User deleted successfully",HttpStatus.ACCEPTED);
    }
	
}
