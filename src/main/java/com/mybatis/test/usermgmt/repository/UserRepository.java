package com.mybatis.test.usermgmt.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mybatis.test.usermgmt.entity.User;

/**
 * DAO class for performing CRUD operation on postgres-sql. 
 * @author JAI
 *
 */

@Mapper
public interface UserRepository {

	@Select("select * from users")
    public List <User> findAll();
	
    @Select("SELECT * FROM users WHERE id = #{id}")
    public User findById(String id);

    @Delete("DELETE FROM users WHERE id = #{id}")
    public int deleteById(String id);

    @Insert("INSERT INTO users(id, created_by, created_time, first_name, last_name, picture, preferred_language, background_check) "
    		+ " VALUES (#{id}, #{createdBy}, #{createdTime}, #{firstName}, #{lastName}, #{picture}, #{preferredLanguage}, #{backgroundCheck})")
    public int addUser(User user);

    @Update("Update users SET last_updated_by=#{lastUpdatedBy}, last_updated_time=#{lastUpdatedTime}, "
    		+ " first_name=#{firstName}, last_name=#{lastName}, picture=#{picture}, preferred_language=#{preferredLanguage}, background_check=#{backgroundCheck}"
    		+ " where id=#{id}")
    public int updateUser(User user);
}
