package com.mybatis.test.usermgmt.service;

import java.util.List;

import com.mybatis.test.usermgmt.entity.User;
import com.mybatis.test.usermgmt.model.UserDTO;

public interface UserService {

	public List <UserDTO> findAll();
	
    public UserDTO findById(String id);

    public int deleteById(String id);

    public int addUser(UserDTO user);

    public int updateUser(UserDTO user);
}
