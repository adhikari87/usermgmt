package com.mybatis.test.usermgmt.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.mybatis.test.usermgmt.entity.User;
import com.mybatis.test.usermgmt.model.UserDTO;
import com.mybatis.test.usermgmt.repository.UserRepository;
import com.mybatis.test.usermgmt.service.UserService;

/**
 * User Service to apply business logic. 
 * @author JAI
 *
 */

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<UserDTO> findAll() {
		List<User> users = userRepository.findAll();
		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
		if(CollectionUtils.isEmpty(users)) {
			for (User user : users) {
				UserDTO userDTO = new UserDTO();
				BeanUtils.copyProperties(user, userDTO);
				userDTOs.add(userDTO);
			}
		}
		return userDTOs;
	}

	@Override
	public int addUser(UserDTO userDTO) {
		userDTO.setCreatedTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		User user = new User();
		BeanUtils.copyProperties(userDTO, user);
		return userRepository.addUser(user);
	}

	@Override
	public UserDTO findById(String id) {
		User user = userRepository.findById(id);
		UserDTO userDTO = null;
		if(user != null) {
			userDTO = new UserDTO();
			BeanUtils.copyProperties(user, userDTO);
		}
		return userDTO;
	}

	@Override
	public int deleteById(String id) {
		return userRepository.deleteById(id);
	}

	@Override
	public int updateUser(UserDTO userDTO) {
		userDTO.setLastUpdatedTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		User user = new User();
		BeanUtils.copyProperties(userDTO, user);
		return userRepository.updateUser(user);

	}
}
