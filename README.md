Steps to run this project-
1.  Clone this project in your local machine.
2.  Create the database in Postgres-
3.  CREATE DATABASE usermgmt WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_India.1252'
    LC_CTYPE = 'English_India.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
3.  Create users table in your DB.
4.  Go to cmd and excute mvn clean install - This will run all the test cases and perform the CRUD operation in DB.
5.  You can execute the CRUD operation from Postman also.


# tw-mybatis-coding-test

This is a coding test for tw backend developer candidates.

#Objective
Use [Mybatis Mapper](http://www.mybatis.org/mybatis-3) to write a basic CRUD application on a User table(users.sql) against a postgres database.

# Instructions to code
*  This repo is readonly, so fork this repo on gitlab and create new repo with public access
*  use spring-boot and maven to give structure to the application
*  use mybatis code-generator to auto-generate the mapping code
*  use junit to write a basic test suite for the CRUD operations
*  update the readme to add accurate instructions on how to setup and run the project and run the tests
*  Share the forked repo link with the hr

#Instructions to run 
TODO by candidate